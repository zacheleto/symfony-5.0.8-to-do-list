# To do list (tasks-list)

A simple TO DO LIST created with Symfony skeleton v5.0.8 and MySQL, where you can add your tasks delete them and check or uncheck them. 

### Prerequisites

none

### Installing

git clone https://github.com/letowebdev/Symfony-5.0.8-To-Do-List.git

## Deployment

https://zacheleto.me/Projects/Symfony_CRUD/public

## Built With

* [Symfony 5.0.8]
* [MySQL]

## Author

* HTML template from W3SCHOOLS resource

source: https://www.w3schools.com/howto/howto_js_todolist.asp  
(modified by Zache Abdelatif to be shared on GITHUB)

## License

This project is licensed under the MIT License
